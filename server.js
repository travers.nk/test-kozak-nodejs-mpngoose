const express = require('express');
const config = require('./config');
const app = express();
const middleware = require('./middleware')(app, express);

const server = app.listen(config.get('port'), function() {
    console.log('Server running at http://localhost:' + server.address().port)
});