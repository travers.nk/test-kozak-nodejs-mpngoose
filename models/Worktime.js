
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const WorktimeSchema = new Schema({
    employer_id: {
        type: Schema.ObjectId,
        ref: 'Employer',
        required: true,
    },

    time_begin :{
        type: String,
    },

    time_end :{
        type: String,
    },

    created_date: {
        type: String,
        default: new Date().toISOString()
    },
});

module.exports = mongoose.model('Worktime', WorktimeSchema);