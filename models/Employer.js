const mongoose = require('mongoose'),
      Schema = mongoose.Schema,
      bcrypt = require('bcrypt');


const EmployerSchema = new Schema({
    first_name: {
        type: String,
        required: true,
        index: { unique: true }
    },

    password: {
        type: String,
        required: true,
    },

    last_name: {
        type: String,
    },

    middle_name: {
        type: String,
    },

    gender: {
        type: String,
    },

    contact_info: {
        type: String,
    },

    created_date: {
        type: String,
        default: new Date().toISOString()
    },

    times: [{ type: Schema.Types.ObjectId, ref: 'Worktime' }]
});

EmployerSchema.pre('save', function (next) {
    let employer = this;
    bcrypt.hash(employer.password, 10, function (err, hash){
        if (err) {
            return next(err);
        }
        employer.password = hash;
        next();
    })
});

module.exports = mongoose.model('Employer', EmployerSchema);