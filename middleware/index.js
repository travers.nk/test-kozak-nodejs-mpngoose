module.exports = function (app, express) {

    const path = require('path'),
    bodyParser = require('body-parser'),
    favicon = require('serve-favicon'),
    errorHandler = require('./error_handler')(app),
    mongoose = require('mongoose');

    mongoose.Promise = global.Promise;
    let dbReady = false;

    mongoose.connect('mongodb://localhost/test_kozak',  {
        useMongoClient: true,
    }).then(
        () => {
            console.log('MongoDB connected');
            dbReady = true;
            },
        (err) => {
        }
    );

    app.use((req, res, next) => {
        if (dbReady) {
            return next();
        }
        next(500);
    });

    require('../models/Employer');
    require('../models/Worktime');

    const routes = require('../routes/indexRoutes');

    app.use(favicon(path.join(__dirname, '../public', 'favicon.ico')));
    app.use('/public', express.static(path.join(__dirname, '../public')));

    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        next();
    });

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json()); // Body parser use JSON data

    app.use('/api/v1/', routes);
    app.use(errorHandler);
}