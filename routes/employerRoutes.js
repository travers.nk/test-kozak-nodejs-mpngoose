/**
 * Created by nataly on 20.11.17.
 */
const router = require('express').Router();
const employerController = require('../controllers/EmployerController');

router.route('/')
    .get(employerController.index)
    .post(employerController.create);

router.route('/:id')
    .get(employerController.detail)
    .put(employerController.edit)
    .delete(employerController.delete);

router.route('/:id/worktime')
    .get(employerController.detailWithTime);

module.exports = router;