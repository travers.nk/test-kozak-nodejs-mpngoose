const router = require('express').Router();
const authController = require('../controllers/AuthController');

router.route('/login')
    .post(authController.authLogin);

router.route('/register')
    .post(authController.authRegister);

module.exports = router;