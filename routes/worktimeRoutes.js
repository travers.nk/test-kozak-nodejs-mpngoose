const router = require('express').Router();
const worktimeController = require('../controllers/WorktimeController');

router.route('/')
    .get(worktimeController.index)
    .post(worktimeController.create);

router.route('/:id')
    .get(worktimeController.detail)
    .put(worktimeController.edit)
    .delete(worktimeController.delete);

module.exports = router;