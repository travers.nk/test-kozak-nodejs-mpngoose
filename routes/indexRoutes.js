/**
 * Created by nataly on 20.11.17.
 */
const employerRoutes = require('./employerRoutes');
const authRoutes = require('./authRoutes');
const worktimeRoutes = require('./worktimeRoutes');
const router = require('express').Router();
const jwt = require('jsonwebtoken');
const config = require('../config/index');

router.use('/auth', authRoutes);

router.use( (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-requested-with'];
    if (req.method !== "OPTIONS"){
        if (token) {
            jwt.verify(token, config.get('secret'), (err, decoded) => {
                if (err) {
                    next(401)
                } else {
                    req.decoded = decoded;
                    next();
                }
            });

        } else {
            return next(403)
        }
    } else next();
});

router.use('/employer', employerRoutes);
router.use('/worktime', worktimeRoutes);

module.exports = router;
