/**
 * Created by nataly on 20.11.17.
 */
const mongoose = require('mongoose'),
    Employer = mongoose.model('Employer'),
    Worktime  = mongoose.model('Worktime'),
    getPayload = require('./payload');


class EmployerController {

     index(req, res, next) {
         const pageOptions = {
             page: req.query.page || 0,
             limit: req.query.limit || 2
         };

         Employer.find({})
             .skip(pageOptions.page*pageOptions.limit)
             .limit(pageOptions.limit)
             .exec((err, employers) => {
                 if (err) {
                     next(404);
                 }
                 else {
                     res.status(200).json(employers);
                 }
             })
    }

    detail(req, res, next){
        Employer.findById(req.params.id, (err, employer) => {
            if (err)
                next(err);
            else
                if (employer)
                res.status(200).json({
                    message: 'success',
                    data: employer
                });
                else next(404);
        });
    }

    edit(req, res, next){
        const payload = getPayload(req);
        Employer.findOneAndUpdate(
            {_id: req.params.id},
            payload,
            {new: true}, (err, employer) => {
            if (err)
                next(err);
            else
                res.status(200).json({
                    message: 'success',
                    data: employer
                });
        });
    }

    create(req, res, next){
        const payload = getPayload(req);
        let  new_employer = new Employer(payload);
        new_employer.save((err, employer) => {
            if (err)
                next(err);
                else {
                res.status(201).json({
                    message: 'success',
                    data: employer
                });
            }
        });
    }

    delete(req, res, next){
        Worktime.findOne({employerId: req.params.id}, (err, result) => {
            if (result){
                res.json(400);
            } else {
                Employer.remove({
                    _id: req.params.id
                }, (err, result) => {
                    if (err)
                        next(err);
                    res.status(204)
                });
            }
        });
    }

    detailWithTime(req, res, next) {
        Employer.
            findById(req.params.id)
            .populate('times')
            .exec((err, employer) => {
            if (err)
                next(err);
            else {
                res.status(200).json({
                    message: 'success',
                    data: employer
                });
            }
        })
     }

}

module.exports = new EmployerController();