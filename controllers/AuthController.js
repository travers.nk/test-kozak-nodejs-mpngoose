
const mongoose = require('mongoose'),
    Employer = mongoose.model('Employer'),
    jwt = require('jsonwebtoken'),
    config = require('../config/index'),
    getPayload = require('./payload'),
    bcrypt = require('bcrypt');


class AuthController {

    authLogin(req, res, next) {
        Employer.findOne({first_name: req.body.first_name}, function (err, employer) {
            if (!employer) {
                next(404)
            } else {
                bcrypt.compare(req.body.password, employer.password, (err, isMatch) => {
                    if (err) {
                        next(401)
                    } else {
                        const token = jwt.sign({
                                gender: employer.gender
                            },
                            config.get('secret'), {
                                expiresIn: '24h'
                            });
                        res.json({
                            message: 'success',
                            token: token,
                            employerId: employer.id
                        });
                    }
                })
            }
        })
    }

    authRegister(req, res, next) {
        const payload = getPayload(req);
        let  new_employer = new Employer(payload);
        new_employer.save((err, employer) => {
            if (err)
                next(err);
            else {
                const token = jwt.sign({
                        gender: employer.occupation
                    },
                    config.get('secret'), {
                        expiresIn: '24h'
                    });
                res.status(201).json({
                    message: 'success',
                    token: token,
                    employer_id: employer.id
                });
            }
        });
    }
}

module.exports = new AuthController();