const mongoose = require('mongoose'),
    Employer = mongoose.model('Employer'),
    Worktime  = mongoose.model('Worktime'),
    getPayload = require('./payload');


class WorktimeController {

    index(req, res, next) {
        const pageOptions = {
            page: req.query.page || 0,
            limit: req.query.limit || 2
        };

        Worktime.find({})
            .skip(pageOptions.page*pageOptions.limit)
            .limit(pageOptions.limit)
            .exec((err, times) => {
                if (err) {
                    next(404);
                }
                else {
                    res.status(200).json(times);
                }
            })
    }

    detail(req, res, next){
        Worktime.findById(req.params.id, (err, time) => {
            if (err)
                next(err);
            else
            if (time)
                res.status(200).json({
                    message: 'success',
                    data: time
                });
            else next(404);
        });
    }

    edit(req, res, next){
        const payload = getPayload(req);
        Worktime.findOneAndUpdate(
            {_id: req.params.id},
            payload,
            {new: true}, (err, time) => {
                if (err)
                    next(err);
                else
                    res.status(200).json({
                        message: 'success',
                        data: time
                    });
            });
    }

    create(req, res, next){
        const payload = getPayload(req);
        let  new_time = new Worktime(payload);
        new_time.save((err, time) => {
            if (err)
                next(err);
            else {
                Employer.findOneAndUpdate(
                    {_id: req.body.employer_id},
                    { $push: { times: new_time._id } },
                    {new: true}, (err, employer) => {
                        if (err)
                            next(err);
                        else

                            res.status(201).json({
                                message: 'success',
                                data: time
                            });
                    });
            }
        });
    }

    delete(req, res, next){
        Worktime.remove({
            _id: req.params.id
        }, (err, result) => {
            if (err)
                next(err);
            res.status(204)
        });
    }
}

module.exports = new WorktimeController();
